//
//  SkillsViewController.swift
//  labbo02
//
//  Created by Ludwig Åkermark on 2019-11-15.
//  Copyright © 2019 Ludwig Åkermark. All rights reserved.
//

import UIKit
class SkillsViewController: UIViewController {

    @IBOutlet weak var animateMe: UIView!
    @IBOutlet weak var dissmissButton: UIButton!
    @IBAction func theAction(_ sender: Any) {

        self.dismiss(animated: true){
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 3.0, delay: 0.0, options: [.curveEaseIn], animations:{
            self.animateMe.backgroundColor = UIColor.red
        }, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
  

}

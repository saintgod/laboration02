//
//  File.swift
//  labbo02
//
//  Created by Ludwig Åkermark on 2019-11-10.
//  Copyright © 2019 Ludwig Åkermark. All rights reserved.
//

import Foundation
class Experience{
    let imageName: String
    let name: String
    let dateTime: String
    let description: String
    init(imageName: String = "defalt", name: String = "Experience", dateTime: String = "2018", description: String = "de_f"){
        self.imageName = imageName
        self.name = name
        self.dateTime = dateTime
        self.description = description
    }
}

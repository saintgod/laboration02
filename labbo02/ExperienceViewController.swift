//
//  ExperienceViewController.swift
//  labbo02
//
//  Created by Ludwig Åkermark on 2019-11-06.
//  Copyright © 2019 Ludwig Åkermark. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExperienceDetailViewController, let exp = sender as? Experience {
            //destination.imgDetail.image = UIImage(systemName: exp.imageName)
            destination.name = exp.name
            destination.time = exp.dateTime
            destination.desc = exp.description
            destination.imgName = exp.imageName
            
            //destination.timeLabel.text = exp.dateTime
            //destination.descLabel.text = exp.description
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return experienceCells.count
        }
        else
        {
            return workCells.count
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let exp: Experience
        if indexPath.section == 0{
             exp = experienceCells[indexPath.row]
        }
        else
        {
            exp = workCells[indexPath.row]
        }
           performSegue(withIdentifier: "detailView", sender: exp)
    }
    	
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "Work"
        }
        else
        {
            return "Education"
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ExpTableViewCell{
                let theList = experienceCells[indexPath.row]
                cell.Label1.text = theList.name
                cell.Label2.text = theList.dateTime
                cell.ImageE.image = UIImage(systemName: theList.imageName)
                return cell
                
            }
        }
        else
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ExpTableViewCell{
                   let theList = workCells[indexPath.row]
                   cell.Label1.text = theList.name
                   cell.Label2.text = theList.dateTime
                   cell.ImageE.image = UIImage(systemName: theList.imageName)
                   return cell
           }
        }
        return UITableViewCell()
    }
    
    @IBOutlet weak var TableView: UITableView!
    var experienceCells: [Experience] = []
    var workCells: [Experience] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        TableView.delegate = self
        TableView.dataSource = self
        let randText = "Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet Lorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismetLorem ipsum example ismet ‹"
        let exp1 = Experience(imageName: "moon.circle", name: "Falköpings Mejeri", dateTime: "2014-2015", description: randText)
        let exp2 = Experience(imageName: "pencil", name: "Söder o Co", dateTime: "2016-2017", description: randText)
        let exp3 = Experience(imageName: "pencil.circle", name: "Store Support", dateTime: "2018-2019", description: randText)
        experienceCells.append(exp1)
        experienceCells.append(exp2)
        experienceCells.append(exp3)
        let work1 = Experience(imageName: "moon.circle", name: "Jönköping University", dateTime: "2018-2021", description: randText)
        workCells.append(work1)
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

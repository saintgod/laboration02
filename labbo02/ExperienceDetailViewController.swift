//
//  ExperienceDetailViewController.swift
//  labbo02
//
//  Created by Ludwig Åkermark on 2019-11-13.
//  Copyright © 2019 Ludwig Åkermark. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {
    var name: String = "name"
    var time: String = "time"
    var desc: String = "desc"
    var imgName: String = "imgMeme"
    @IBOutlet weak var imgDetail: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = name
        timeLabel.text = time
        descLabel.text = desc
        imgDetail.image = UIImage(systemName: imgName)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

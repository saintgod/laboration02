//
//  ExpTableViewCell.swift
//  labbo02
//
//  Created by Ludwig Åkermark on 2019-11-06.
//  Copyright © 2019 Ludwig Åkermark. All rights reserved.
//

import UIKit

class ExpTableViewCell: UITableViewCell {

    @IBOutlet weak var ImageE: UIImageView!
    @IBOutlet weak var Label1: UILabel!
    @IBOutlet weak var Label2: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
